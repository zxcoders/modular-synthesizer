const int analogInPin = A0;
int sensorValue = 0;
int counter = 0;


void setup() {
  Serial.begin(115200);
  pinMode(5, OUTPUT); //freq 1/1
  pinMode(4, OUTPUT); //freq 1/2
}

void loop() {
  sensorValue = analogRead(analogInPin)*10;
  counter++;
  if (counter >= sensorValue) {
    if(digitalRead(5) == LOW){
       switchState(4);
    }
    switchState(5);
    counter = 0;
  }
}

void switchState(int pin) {
  if (digitalRead(pin) == LOW) {
      digitalWrite(pin, HIGH);  
    } else {
      digitalWrite(pin, LOW);
    }
}